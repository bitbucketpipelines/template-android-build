package com.atlassian.template

import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect1() {
        Thread.sleep(1000) // artificially delay test execution
        assertEquals(4, 2 + 2)
    }
    @Test
    fun addition_isCorrect2() {
        Thread.sleep(1000) // artificially delay test execution
        assertEquals(4, 2 + 2)
    }
    @Test
    fun addition_isCorrect3() {
        Thread.sleep(1000) // artificially delay test execution
        assertEquals(4, 2 + 2)
    }
    @Test
    fun addition_isCorrect4() {
        Thread.sleep(1000) // artificially delay test execution
        assertEquals(4, 2 + 2)
    }
    @Test
    fun addition_isCorrect5() {
        Thread.sleep(1000) // artificially delay test execution
        assertEquals(4, 2 + 2)
    }
    @Test
    fun addition_isCorrect6() {
        Thread.sleep(1000) // artificially delay test execution
        assertEquals(4, 2 + 2)
    }
    @Test
    fun addition_isCorrect7() {
        Thread.sleep(1000) // artificially delay test execution
        assertEquals(4, 2 + 2)
    }
    @Test
    fun addition_isCorrect8() {
        Thread.sleep(1000) // artificially delay test execution
        assertEquals(4, 2 + 2)
    }
    @Test
    fun addition_isCorrect9() {
        Thread.sleep(1000) // artificially delay test execution
        assertEquals(4, 2 + 2)
    }
    @Test
    fun addition_isCorrect10() {
        Thread.sleep(1000) // artificially delay test execution
        assertEquals(4, 2 + 2)
    }
}